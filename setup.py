"""
Setup for AWS Utils
"""

from setuptools import setup


setup(name='aws-utils',
      version='1.0',
      description='AWS utilities',
      url='https://gitlab.com/merfrei/aws-utils',
      author='Emiliano M. Rudenick',
      author_email='erude@merfrei.com',
      license='MIT',
      packages=['aws'],
      install_requires=[
          'boto3',
      ],
      zip_safe=False)
